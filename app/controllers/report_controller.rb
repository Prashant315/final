class ReportController < ApplicationController

    def index
        @orders = Order.all
        @members = Member.all
        @items = Item.all
        @active = Order.active?
        @expired = Order.expired?
    end
end
